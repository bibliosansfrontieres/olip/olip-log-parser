#!/usr/bin/python
import time
import os
from watchdog.observers import Observer
from FileWatcher import FileWatcher
from utils import banner, display_current_env
from files import paths  # ALL FILES PATHS TO READ/WRITE
from files import check_archivefile, check_statefile

if __name__ == "__main__":
    banner()
    check_archivefile()
    check_statefile()
    display_current_env()
    file_watcher = FileWatcher()
    observer = Observer()
    observer.schedule(file_watcher, path=paths['mainLogPath'], recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
