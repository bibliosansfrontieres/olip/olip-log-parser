# ARCHIVED

This project has been replaced by [goAccess](https://gitlab.com/bibliosansfrontieres/olip/olip-deploy/-/compare/master...feature%2Fenvoy_support)

# LOGPARSER

Environment vars :
- STATEFILE_PATH (ex: "./files/statefile.json")
- ARCHIVEFILE_PATH (ex: "./files/archivefile.json")
- LOGFORMAT_PATH (ex: "./files/logformat")
- NGINX_LOG_PATH (ex : /var/log/nginx/access.log)
- NGINX_LOG_FILE (ex : /var/log/nginx)

# Install

Install it via the ansible playbook available in the ansible branch of the repo.
Requires to be run with python3.* 

# How it works

This parser watches an NGINX logfile **NGINX_LOG_FILE** in **NGINX_LOG_PATH**, each time this file is written in it triggers a parsing cycle for the parser.
A parsing cycle is initialized by the statefile **STATEFILE_PATH**, in which we can find where the reading cursor was last in the logfile, and the current daily gathered data.
The cycle ends when the cursor has reached the end of the logfile.
During a parsing cycle, the parser uses a logformat **LOGFORMAT_PATH** to have the right key/values for each lines of log. This can be found in the NGINX configuration /etc/nginx/nginx.conf under logformat.
When the parser notices a logline whose date indicates a new day, the daily gathered data is written to the archive file **ARCHIVEFILE_PATH**.
