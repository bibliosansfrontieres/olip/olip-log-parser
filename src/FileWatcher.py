from watchdog.events import FileSystemEventHandler
from DailyLog import DailyLog
from files import paths

class FileWatcher(FileSystemEventHandler):

    def __init__(self):
        self.daily = DailyLog()

    def on_modified(self, event):
        if event.src_path == paths['mainLogFile']:
            self.daily.analyzeLines()

    def on_created(self, event):
        if event.src_path == paths['mainLogFile']:
            self.daily.fileCreated()
