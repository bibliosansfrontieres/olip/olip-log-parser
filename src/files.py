from os import getenv, path, makedirs
import json
from defaultStatefile import default_statefile

archivefilePath = getenv("ARCHIVEFILE_PATH", "./files/archivefile.json")
statefilePath = getenv("STATEFILE_PATH", "./files/statefile.json")
nginxLogFormatPath = getenv("LOGFORMAT_PATH", "./files/logformat")
mainLogFile = getenv("NGINX_LOG_FILE", "/var/log/nginx/access.log")
mainLogPath = getenv("NGINX_LOG_PATH", "/var/log/nginx")

paths = {
    "archivefilePath": archivefilePath,
    "statefilePath": statefilePath,
    "nginxLogFormatPath": nginxLogFormatPath,
    "mainLogFile": mainLogFile,
    "mainLogPath": mainLogPath
}

def check_archivefile():
    if path.isfile(archivefilePath):
        with open(archivefilePath, "r") as archivefile:
            archive = json.load(archivefile)
        if (not archive):
            with open(archivefilePath, "w+") as archivefile:
                archivefile.write("{\"logs\":[]}")
    else:
        with open(archivefilePath, "w+") as archivefile:
            pass

def check_statefile():
    if path.isfile(statefilePath):
        with open(statefilePath, "r+") as statefile:
            state = json.load(statefile)
        if (not state):
            state = default_statefile
    else:
        with open(statefilePath, "w+") as statefile:
            state = default_statefile
            json.dump(state, statefile)


############################
### READ/WRITE FUNCTIONS ###
############################

# Gets current state from statefile
def get_state():
    with open(paths['statefilePath'], "r") as statefile:
        state = json.load(statefile)
    return state

# Gets last daily log from statefile
def get_state_dailylog():
    with open(paths['statefilePath'], "r") as statefile:
        state = json.load(statefile)
    return state['lastDailyLog']

# Reads archive from archivefile
def get_current_archive():
    with open(paths['archivefilePath'], "r") as archivefile:
        archive = json.load(archivefile)
    return archive

def write_json_statefile(newState):
    with open(paths['statefilePath'], "w") as statefile:
        json.dump(newState, statefile, indent=2)

def write_json_archivefile(newArchive):
    with open(paths['archivefilePath'], "w") as archivefile:
        json.dump(newArchive, archivefile, indent=2)



