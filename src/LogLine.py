from files import paths
from argparser import verbose

class LogLine:
    def __init__(self):
        self.logSeparator = ""
        self.logFormatKeys = self.get_log_format_keys()
        
    # Parses log format from logformat file
    def get_log_format_keys(self):
        with open(paths['nginxLogFormatPath'], "r") as logFormatFile:
            logFormatKeys = logFormatFile.readline()
            logSeparator = logFormatFile.readline().split('=')[1]
            self.logSeparator = logSeparator
            logFormatKeys = logFormatKeys.split(logSeparator)
            logFormatKeys = [el[1:] for el in logFormatKeys]
            logFormatKeys[-1] = logFormatKeys[-1][:-1]
        verbose("[*] LOGFORMAT KEYS FOUND :", 2)
        verbose(logFormatKeys, 2)
        return logFormatKeys


    ## Generic function to get a field from log line
    def get_logline_value(self, logLine, key):
        if key in self.logFormatKeys:
            index = self.logFormatKeys.index(key)
        verbose("[*] LOG LINE :", 2)
        verbose("[*] " + logLine, 2)
        return logLine.split(self.logSeparator)[index]

    # Below are field-specific functions
    def get_status_is_success(self, logLine):
        status = self.get_logline_value(logLine, 'status')
        verbose("[*] STATUS :", 2)
        verbose("[*] " + status, 2)
        verbose("[*] IS VALID? :" + str(status[0]
                                        == '2' or status[0] == '3'), 2)
        return (status[0] == '2' or status[0] == '3')

    def get_log_date(self, logLine): #2019-08-19T15:04:06+01:00
        fulldate = self.get_logline_value(logLine, 'time_local')
        date = fulldate.split('T')[0]
        verbose("[*] DATE :", 2)
        verbose("[*] " + date, 2)
        return date

    def get_log_request(self, logLine): 
        request = self.get_logline_value(logLine, 'request')
        return request #LOOKS LIKE : "GET /img/bg-menu.dd517ad5.png HTTP/1.1"

    def get_log_method(self, logLine):
        method = self.get_log_request(logLine).split(' ')[0]
        return method

    def get_log_path_accessed(self, logLine):
        path = self.get_log_request(logLine).split(' ')[1]
        return path

    def get_remote_addr(self, logLine):
        remote_addr = self.get_logline_value(logLine, 'remote_addr')
        return remote_addr
    