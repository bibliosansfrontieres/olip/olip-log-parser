#!/bin/bash

wrkdir=/home/theoperso/tmp

function addLogLine(){
	rand=$(shuf -i1-3 -n1)
	if [ "$rand" -eq "1" ]; then
		echo "192.168.6.223-192.168.6.213-06/Aug/2019:14:20:16 +0100-GET /js HTTP/1.1-304-0-http://192.168.6.223/home-Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0-80" >> access.log
	fi
	if [ "$rand" -eq "2" ]; then
		echo "192.168.6.223-192.168.6.213-06/Aug/2019:14:20:16 +0100-GET /home HTTP/1.1-304-0-http://192.168.6.223/home-Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0-80" >> access.log
	fi
	if [ "$rand" -eq "3" ]; then
		echo "192.168.6.223-192.168.6.213-06/Aug/2019:14:20:16 +0100-GET /applications HTTP/1.1-404-0-http://192.168.6.223/home-Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0-80" >> access.log
	fi
}

while [ 1 ]; do
	i=0
	while [ $i -lt 30 ]; do
		addLogLine
		echo "ADD LOG $i"
		sleep 0.2
		i=$((i+1))
	done
	echo "ROTATE"
	mv $wrkdir/access.log $wrkdir/access.log.1
	touch $wrkdir/access.log
done
