import re
from files import paths, archivefilePath, statefilePath, nginxLogFormatPath, mainLogFile, mainLogPath

def ignored(line):
    # If log line matches our regex, print to console, and output file
    ignored_paths = ["/css", "/js", "/img", "/config.json", "/statics", "/fonts", "/ ", "/favicon", "/oidc-callback", "/installed", "/report"]
    line_should_be_ignored = False
    for path in ignored_paths:
        if re.findall(path, line):
            line_should_be_ignored = True
            break
    return line_should_be_ignored

def banner():
    print("    ___  ________        __   ____  ________  ___   ___  ___________     ")
    print("   / _ )/ __/ __/ ____  / /  / __ \/ ___/ _ \/ _ | / _ \/ __/ __/ _ \    ")
    print("  / _  |\ \/ _/  /___/ / /__/ /_/ / (_ / ___/ __ |/ , _/\ \/ _// , _/    ")
    print(" /____/___/_/         /____/\____/\___/_/  /_/ |_/_/|_/___/___/_/|_|     ")                                                             

def display_current_env():
    print("                       ")
    print(" - NGINX_LOG_PATH     : " + mainLogPath)
    print(" - NGINX_LOG_FILE     : " + mainLogFile)
    print(" - ARCHIVEFILE_PATH   : " + archivefilePath)
    print(" - STATEFILE_PATH     : " + statefilePath)
    print(" - LOGFORMAT_PATH     : " + nginxLogFormatPath)