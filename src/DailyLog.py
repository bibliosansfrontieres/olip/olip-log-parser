import os
import json
from utils import ignored
from argparser import verbose
import re
from LogLine import LogLine
from files import paths
from files import get_state, get_state_dailylog, get_current_archive, write_json_statefile, write_json_archivefile
from defaultDataModel import default_data_model

##########################
##### DAILYLOG CLASS #####
##########################
# State is to be read from statefile
# Purpose of this class is to store daily log result,
# regularly saving it via statefile, and then adding
# it to archivefile.

class DailyLog:
    def __init__(self):
        state = get_state()
        self.data = {
            'nbSuccessRequests': state['lastDailyLog']['nbSuccessRequests'],
            'nbFailedRequests': state['lastDailyLog']['nbFailedRequests'],
            'accessedRoutes': state['lastDailyLog']['accessedRoutes'],
            'remote_addresses': state['lastDailyLog']['remote_addresses']
        }
        self.cursor = state['cursor']
        self.currentDate = state['lastDailyLog']['date']
        self.logLineParser = LogLine()

    ##########################
    ### READ/WRITE METHODS ###
    ##########################

    # Writes class state to statefile
    def write_to_statefile(self):
        newState = get_state()
        newState['lastDailyLog'] = self.data
        newState['cursor'] = self.cursor
        newState['lastDailyLog']['date'] = self.currentDate
        write_json_statefile(newState)

    # Writes new dailylog to archivefile
    def write_to_archive(self):
        newArchive = get_current_archive()
        newDailyLog = self.data
        newDailyLog['date'] = self.currentDate
        newArchive['logs'].append(newDailyLog)
        write_json_archivefile(newArchive)

    ##############################
    ### INFO GATHERING METHODS ###
    ##############################

    # Function called when a date change has been detected
    def date_has_changed(self, newDate):
        verbose("[*] DATE CHANGED")
        self.write_to_archive()
        self.set_date(newDate)
        self.reset_data()

    # Function called when a file has been created in the log folder
    def fileCreated(self):
        verbose("[****] NEW FILE - ROTATION SUPPOSED")
        self.reset_cursor()
        self.write_to_statefile()

    # Function called when the main log file has been written in
    # It triggers a parsing cycle
    def analyzeLines(self):
        verbose("[***] ANALYZING SAME FILE...")
        filePath = paths['mainLogFile']
        with open(filePath, "r") as logFile:
            logFile.seek(self.cursor)
            logLine = logFile.readline()
            while logLine:
                self.analyzeLine(logLine)
                self.cursor = logFile.tell()
                self.write_to_statefile()
                logLine = logFile.readline()

    # Function called multiple times by analyzeLines()
    # It gathers the info on a logline
    def analyzeLine(self, logLine):
        verbose("[**] NEW LOGLINE")
        if (ignored(logLine)):
            verbose("[*] LINE IGNORED: UNINTERESTING ACCESS PATH", 2)
            return 0
        date = self.logLineParser.get_log_date(logLine)

        if self.currentDate != date:
            self.date_has_changed(date)

        ### REQUEST STATUS
        if self.logLineParser.get_status_is_success(logLine):
            self.data['nbSuccessRequests'] += 1
        else:
            self.data['nbFailedRequests'] += 1

        ### ACCESSED ROUTES
        route = self.logLineParser.get_log_path_accessed(logLine)
        if (route in self.data['accessedRoutes']):
            self.data['accessedRoutes'][route] += 1
        else:
            self.data['accessedRoutes'][route] = 1

        ### IPs ACCESSING RESSOURCES
        remote_addr = self.logLineParser.get_remote_addr(logLine)
        if (remote_addr in self.data['remote_addresses']):
            self.data['remote_addresses'][remote_addr] += 1
        else:
            self.data['remote_addresses'][remote_addr] = 1

        

    ###########################
    ### CLASS DATA HANDLING ###
    ###########################

    def reset_data(self):
        self.data = default_data_model

    def set_date(self, newDate):
        self.currentDate = newDate

    def reset_cursor(self):
        self.cursor = 0
