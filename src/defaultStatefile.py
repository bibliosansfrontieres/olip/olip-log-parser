################################################
# USED AS DEFAULT VALUE IF STATEFILE NOT FOUND #
################################################

default_statefile = {
    "cursor": 0,
    "lastDailyLog": {
        "date": "29/Jul/2019",
        "nbFailedRequests": 0,
        "nbSuccessRequests": 0,
        "accessedRoutes":{
        
        },
        "remote_addresses":{
            
        }
    }
}