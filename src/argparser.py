import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-v',
                    help='Toggles verbose mode',
                    action="store_true")
parser.add_argument('-vv',
                    help='Toggles VERY verbose mode',
                    action="store_true")
args = parser.parse_args()

if args.v:
    print("[***] VERBOSE MODE ON !")
if args.vv:
    print("[***] VERY VERBOSE MODE ON !")


def verbose(message, verbosity=1):
    if (args.v or args.vv) and verbosity == 1:
        print(message)
    if args.vv and verbosity == 2:
        print(message)
